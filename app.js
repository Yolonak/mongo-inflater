const express = require('express');
const { Connection } = require('./db/Connection');

const port = process.env.serverPORT || 7000;
const nqueries = parseInt(process.env.nqueries, 10) || 10;
const ncopies = parseInt(process.env.ncopies, 10) || 10;
const app = express();

app.use(express.json());

async function queryLoop(database, n, m = 10)
{
    let c = 0;
    let i = 0;
    while (c < n)
    {
        try
        {
            // eslint-disable-next-line no-await-in-loop
            await database.collection('tickets_reporting').aggregate([
                { $set: { xxx: { $range: [0, m] } } },
                { $unwind: { path: '$xxx' } },
                { $project: { _id: 0, xxx: 0 } },
                {
                    $merge:
                    {
                        into: 'newCollection',
                        on: '_id',
                        whenMatched: 'replace',
                        whenNotMatched: 'insert',
                    },
                }]).toArray();
            i += 1;
            console.log(i, ' inserted');
        }
        catch (error)
        {
            console.warn(error);
        }
        finally
        {
            c += 1;
        }
    }
    console.log(i, '/', c, ' inserted');
}

const server = app.listen(port, async () =>
{
    await Connection.open();

    // console.log(process.env.dbName);
    const database = Connection.db.db(process.env.dbName);

    console.info(`Server started on port ${port}`);
    await queryLoop(database, nqueries, ncopies);
});
